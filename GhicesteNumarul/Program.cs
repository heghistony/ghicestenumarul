﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercitiulNr1BUN
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int aleator = rand.Next(100);
            int numar;
            int numarIncercari = 0;
            do
            {
                numarIncercari++;
                if (!int.TryParse(Console.ReadLine(), out numar))
                {
                    Console.WriteLine("Numarul introdus este gresit!");
                }
                else if (aleator == numar)
                {
                    Console.WriteLine("BRAVOO! Ati ghicit numarul!");
                    Console.WriteLine($"Numarul de incercari a fost de {numarIncercari}!");
                }
                else if (Math.Abs(numar - aleator) <= 3)
                {
                    Console.WriteLine("Foarte fierbinte!");
                }
                else if (Math.Abs(numar - aleator) <= 5)
                {
                    Console.WriteLine("Fierbinte!");
                }
                else if (Math.Abs(numar - aleator) <= 10)
                {
                    Console.WriteLine("Cald!");
                }
                else if (Math.Abs(numar - aleator) <= 20)
                {
                    Console.WriteLine("Caldut!");
                }
                else if (Math.Abs(numar - aleator) <= 50)
                {
                    Console.WriteLine("Rece!");
                }
                else
                {
                    Console.WriteLine("Foarte rece!");
                }
            } while (numar != aleator);

            Console.ReadKey();
        }
    }
}